import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useLoadingStore } from './loading'
import http from '@/services/http'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  async function callConvert() {
    console.log('Store: call Convert')
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
    console.log('Store: finish call Convert')
  }

  return { valid, celsius, result, callConvert }
})
