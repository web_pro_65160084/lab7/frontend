import type { User } from "@/types/User";
import http from "./http";

async function addUser(user: User) {
    return http.post('/users', user)
}

async function updateUser(user: User) {
    return http.patch(`/users/${user.id}`, user)
}

async function delUser(user: User) {
    return http.delete(`/users/${user.id}`)
}

async function getUser(id: number) {
    return http.get(`/users/${id}`)
}

async function getUsers() {
    return http.get('/users')
}

export default { addUser, updateUser, delUser, getUser, getUsers }